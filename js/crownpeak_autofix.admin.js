(function ($, Drupal) {
  Drupal.behaviors.crownpeakAutofix = {
    attach: function (context, settings) {
      $('#crownpeak-autofix-preview', context).click(function () {
        crownpeakAutofix.destroy(context);
        crownpeakAutofix.init(context);
      });

      $('#edit-trigger-button-type', context).change(function () {
        crownpeakAutofix.destroy(context);
      });
    }
  };

  let crownpeakAutofix = {};
  crownpeakAutofix.init = function (context) {
    let configs = crownpeakAutofix.getConfigs(context);
    crownpeakAutofix.addCrownpeakJS(configs);
  }

  crownpeakAutofix.destroy = function (context) {
    if (typeof acsbJS !== 'undefined') {
      acsbJS.destroy();
    }
  }

  crownpeakAutofix.getConfigs = function (context) {
    let configs = {
      statementLink : $('#edit-accessibility-statement-link', context).val(),
      feedbackLink : $('#edit-feedback-form-link', context).val(),
      footerHtml : '',
      hideMobile : ($('#edit-hide-on-mobile', context).val() === 'true'),
      hideTrigger : false,
      language : $('#edit-interface-language', context).val(),
      position : $('#edit-interface-position', context).val(),
      leadColor : $('#edit-interface-lead-color', context).val(),
      triggerColor : $('#edit-trigger-button-color', context).val(),
      triggerRadius : $('#edit-trigger-button-shape', context).val(),
      triggerPositionX : $('#edit-trigger-horizontal-position', context).val(),
      triggerPositionY : $('#edit-trigger-vertical-position', context).val(),
      triggerIcon : $('input[name="trigger_button_icon"]:checked', context).val(),
      triggerSize : $('#edit-trigger-button-size', context).val(),
      triggerOffsetX : $('#edit-trigger-horizontal-offset', context).val(),
      triggerOffsetY : $('#edit-trigger-vertical-offset', context).val(),
      mobile : {
        triggerSize: $('#edit-mobile-trigger-size', context).val(),
        triggerPositionX : $('#edit-mobile-trigger-horizontal-position', context).val(),
        triggerPositionY : $('#edit-mobile-trigger-vertical-position', context).val(),
        triggerOffsetX : $('#edit-mobile-trigger-horizontal-offset', context).val(),
        triggerOffsetY : $('#edit-mobile-trigger-vertical-offset', context).val(),
        triggerRadius : $('#edit-mobile-trigger-shape', context).val()
      }
    }

    return configs;
  }

  crownpeakAutofix.addCrownpeakJS = function (configs) {
    var s = document.createElement('script'),
      e = ! document.body ? document.querySelector('head') : document.body;
    s.src = 'https://acsbap.com/apps/app/assets/js/acsb.js';
    s.async = s.defer = true;
    s.onload = function () {
      acsbJS.init(configs);
    };
    e.appendChild(s);
  }

})(jQuery, Drupal);

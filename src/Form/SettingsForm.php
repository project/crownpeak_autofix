<?php

namespace Drupal\crownpeak_autofix\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures integration with crownpeak autofix service.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Stores the state storage service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state, FileUrlGeneratorInterface $file_url_generator) {
    parent::__construct($config_factory);
    $this->state = $state;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'crownpeak_autofix_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'crownpeak_autofix.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // For form elements check:
    // https://developer.crownpeak.com/autofix/Generatescript.html
    $configs = $this->config('crownpeak_autofix.settings');

    $form['interface'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Interface Settings'),
    ];

    $form['interface']['interface_language'] = [
      '#type' => 'select',
      '#title' => $this->t('Interface Language'),
      '#description' => $this->t('Select language for widget interface.'),
      '#default_value' => $configs->get('interface_language'),
      '#options' => [
        'en' => $this->t('English'),
        'es' => $this->t('Spanish'),
        'fr' => $this->t('French'),
        'de' => $this->t('German'),
        'it' => $this->t('Italian'),
        'pt' => $this->t('Portuguese'),
        'nl' => $this->t('Dutch'),
        'ja' => $this->t('Japanese'),
        'tw' => $this->t('Chinese (tw)'),
        'zh' => $this->t('Chinese (zh)'),
        'he' => $this->t('Hebrew'),
        'ru' => $this->t('Russian'),
        'ar' => $this->t('Arabic (ar)'),
        'ua' => $this->t('Arabic (ua)'),
        'pl' => $this->t('Polish'),
        'tr' => $this->t('Turkish'),
      ],
      '#required' => TRUE,
    ];

    $form['interface']['interface_lead_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Interface Lead Color'),
      '#description' => $this->t('Specify lead color for widget interface.'),
      '#default_value' => $configs->get('interface_lead_color'),
      '#required' => TRUE,
    ];

    $form['interface']['interface_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Interface Position'),
      '#description' => $this->t('Select showing trigger button option.'),
      '#default_value' => $configs->get('interface_position'),
      '#options' => [
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ],
      '#required' => TRUE,
    ];

    $form['interface']['links_configs'] = [
      '#type' => 'details',
      '#title' => $this->t('Interface Links'),
    ];

    $form['interface']['links_configs']['accessibility_statement_link'] = [
      '#type' => 'url',
      '#title' => $this->t('Accessibility Statement Link'),
      '#description' => $this->t('Specify here URL for page with accessibility statement.'),
      '#default_value' => $configs->get('accessibility_statement_link'),
    ];

    $form['interface']['links_configs']['feedback_form_link'] = [
      '#type' => 'url',
      '#title' => $this->t('Feedback Form Link'),
      '#description' => $this->t('Specify here URL for feedback form.'),
      '#default_value' => $configs->get('feedback_form_link'),
    ];

    $form['trigger'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Trigger Settings'),
    ];

    $form['trigger']['trigger_button_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Trigger Button Type'),
      '#description' => $this->t('Select trigger button type.'),
      '#default_value' => $configs->get('trigger_button_type'),
      '#options' => [
        CROWNPEAK_AUTOFIX_TRIGGER_BUTTON_TYPE_CROWNPEAK => $this->t('Crownpeak'),
        CROWNPEAK_AUTOFIX_TRIGGER_BUTTON_TYPE_BLOCK => $this->t('Block'),
        CROWNPEAK_AUTOFIX_TRIGGER_BUTTON_TYPE_CUSTOM => $this->t('Custom HTML'),
      ],
      '#required' => TRUE,
    ];

    $form['trigger']['trigger_preview'] = [
      '#type' => 'container',
      '#markup' => '<a href="#" id="crownpeak-autofix-preview" title="' . $this->t('Preview Crownpeak Autofix Trigger and Interface') . '">' . $this->t('Preview') . '</a>',
      '#states' => [
        'visible' => [
          ':input[name="trigger_button_type"]' => ['value' => CROWNPEAK_AUTOFIX_TRIGGER_BUTTON_TYPE_CROWNPEAK],
        ],
      ],
    ];

    $form['trigger']['crownpeak_trigger'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Crownpeak Trigger'),
      '#states' => [
        'visible' => [
          ':input[name="trigger_button_type"]' => ['value' => CROWNPEAK_AUTOFIX_TRIGGER_BUTTON_TYPE_CROWNPEAK],
        ],
      ],
    ];

    $form['trigger']['crownpeak_trigger']['trigger_button_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Trigger Button Color'),
      '#description' => $this->t('Specify lead color for trigger button.'),
      '#default_value' => $configs->get('trigger_button_color'),
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['trigger_horizontal_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Trigger Horizontal Position'),
      '#description' => $this->t('Select horizontal position for trigger.'),
      '#default_value' => $configs->get('trigger_horizontal_position'),
      '#options' => [
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ],
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['trigger_vertical_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Trigger Vertical Position'),
      '#description' => $this->t('Select vertical position for trigger.'),
      '#default_value' => $configs->get('trigger_vertical_position'),
      '#options' => [
        'top' => $this->t('Top'),
        'center' => $this->t('Center'),
        'bottom' => $this->t('Bottom'),
      ],
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['trigger_button_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Trigger Button Size'),
      '#description' => $this->t('Select button size for trigger.'),
      '#default_value' => $configs->get('trigger_button_size'),
      '#options' => [
        'small' => $this->t('Small'),
        'medium' => $this->t('Medium'),
        'big' => $this->t('Big'),
      ],
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['trigger_button_shape'] = [
      '#type' => 'select',
      '#title' => $this->t('Trigger Button Shape'),
      '#description' => $this->t('Select button shape for trigger button.'),
      '#default_value' => $configs->get('trigger_button_shape'),
      '#options' => [
        '50%' => $this->t('Round'),
        '0%' => $this->t('Square'),
        '10px' => $this->t('Squircle Big'),
        '5px' => $this->t('Squircle Small'),
      ],
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['trigger_button_icon'] = [
      '#type' => 'radios',
      '#title' => $this->t('Trigger Button Icon'),
      '#description' => $this->t('Select button icon for trigger.'),
      '#default_value' => $configs->get('trigger_button_icon'),
      '#options' => [
        'display' => $this->t('Display'),
        'display2' => $this->t('Display 2'),
        'display3' => $this->t('Display 3'),
        'help' => $this->t('Help'),
        'people' => $this->t('People'),
        'people2' => $this->t('People 2'),
        'settings' => $this->t('Settings'),
        'settings2' => $this->t('Settings 2'),
        'wheels' => $this->t('Wheels'),
        'wheels2' => $this->t('Wheels 2'),
      ],
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['trigger_horizontal_offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Trigger Horizontal Offset'),
      '#description' => $this->t('Specify horizontal offset.'),
      '#default_value' => $configs->get('trigger_horizontal_offset'),
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['trigger_vertical_offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Trigger Vertical Offset'),
      '#description' => $this->t('Specify vertical offset.'),
      '#default_value' => $configs->get('trigger_vertical_offset'),
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['mobile_configs'] = [
      '#type' => 'details',
      '#title' => $this->t('Mobile Configuration'),
    ];

    $form['trigger']['crownpeak_trigger']['mobile_configs']['hide_on_mobile'] = [
      '#type' => 'select',
      '#title' => $this->t('Hide On Mobile?'),
      '#description' => $this->t('Select if widget should be shown for mobile devices.'),
      '#default_value' => $configs->get('hide_on_mobile'),
      '#options' => [
        'false' => $this->t('Show'),
        'true' => $this->t('Hide'),
      ],
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['mobile_configs']['details'] = [
      '#type' => 'fieldset',
      '#states' => [
        'visible' => [
          ':input[name="hide_on_mobile"]' => ['value' => 'false'],
        ],
      ],
    ];

    $form['trigger']['crownpeak_trigger']['mobile_configs']['details']['mobile_trigger_horizontal_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Mobile Trigger Horizontal Position'),
      '#description' => $this->t('Select horizontal position for trigger on mobile devices.'),
      '#default_value' => $configs->get('mobile_trigger_horizontal_position'),
      '#options' => [
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ],
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['mobile_configs']['details']['mobile_trigger_vertical_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Mobile Trigger Vertical Position'),
      '#description' => $this->t('Select vertical position for trigger.'),
      '#default_value' => $configs->get('mobile_trigger_vertical_position'),
      '#options' => [
        'top' => $this->t('Top'),
        'center' => $this->t('Center'),
        'bottom' => $this->t('Bottom'),
      ],
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['mobile_configs']['details']['mobile_trigger_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Mobile Trigger Size'),
      '#description' => $this->t('Select button size for trigger on mobile devices.'),
      '#default_value' => $configs->get('mobile_trigger_size'),
      '#options' => [
        'small' => $this->t('Small'),
        'medium' => $this->t('Medium'),
        'big' => $this->t('Big'),
      ],
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['mobile_configs']['details']['mobile_trigger_shape'] = [
      '#type' => 'select',
      '#title' => $this->t('Trigger Mobile Shape'),
      '#description' => $this->t('Select button shape for trigger button on mobile devices.'),
      '#default_value' => $configs->get('mobile_trigger_shape'),
      '#options' => [
        '50%' => $this->t('Round'),
        '0%' => $this->t('Square'),
        '10px' => $this->t('Squircle Big'),
        '5px' => $this->t('Squircle Small'),
      ],
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['mobile_configs']['details']['mobile_trigger_horizontal_offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Trigger Horizontal Offset'),
      '#description' => $this->t('Specify horizontal offset on mobile devices.'),
      '#default_value' => $configs->get('mobile_trigger_horizontal_offset'),
      '#required' => TRUE,
    ];

    $form['trigger']['crownpeak_trigger']['mobile_configs']['details']['mobile_trigger_vertical_offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Trigger Vertical Offset'),
      '#description' => $this->t('Specify vertical offset on mobile devices.'),
      '#default_value' => $configs->get('mobile_trigger_vertical_offset'),
      '#required' => TRUE,
    ];

    // Visibility settings.
    $form['display_settings'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Trigger Display Settings'),
      '#states' => [
        'visible' => [
          [':input[name="trigger_button_type"]' => ['value' => CROWNPEAK_AUTOFIX_TRIGGER_BUTTON_TYPE_CROWNPEAK]],
          'or',
          [':input[name="trigger_button_type"]' => ['value' => CROWNPEAK_AUTOFIX_TRIGGER_BUTTON_TYPE_CUSTOM]],
        ],
      ],
    ];

    $form['display']['page_visibility_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Pages'),
      '#group' => 'display_settings',
      '#states' => [
        'visible' => [
          [':input[name="trigger_button_type"]' => ['value' => CROWNPEAK_AUTOFIX_TRIGGER_BUTTON_TYPE_CROWNPEAK]],
          'or',
          [':input[name="trigger_button_type"]' => ['value' => CROWNPEAK_AUTOFIX_TRIGGER_BUTTON_TYPE_CUSTOM]],
        ],
      ],
    ];

    $form['display']['page_visibility_settings']['visibility_request_path_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add widget to specific pages'),
      '#options' => [
        $this->t('Every page except the listed pages'),
        $this->t('The listed pages only'),
      ],
      '#default_value' => $configs->get('visibility_request_path_mode'),
    ];

    $visibility_request_path_pages = $configs->get('visibility_request_path_pages');
    $form['display']['page_visibility_settings']['visibility_request_path_pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#title_display' => 'invisible',
      '#default_value' => !empty($visibility_request_path_pages) ? $visibility_request_path_pages : '',
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.",
        [
          '%blog' => '/blog',
          '%blog-wildcard' => '/blog/*',
          '%front' => '<front>',
        ]
      ),
      '#rows' => 10,
    ];

    $form['display']['role_visibility_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles'),
      '#group' => 'display_settings',
    ];

    $form['display']['role_visibility_settings']['visibility_user_role_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add widget for specific roles'),
      '#options' => [
        $this->t('Add to the selected roles only'),
        $this->t('Add to every role except the selected ones'),
      ],
      '#default_value' => $configs->get('visibility_user_role_mode'),
    ];

    $visibility_user_role_roles = $configs->get('visibility_user_role_roles');
    $form['display']['role_visibility_settings']['visibility_user_role_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#default_value' => !empty($visibility_user_role_roles) ? $visibility_user_role_roles : [],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', user_role_names()),
      '#description' => $this->t('If none of the roles are selected, widget will be shown for all users. If a user has any of the roles checked, widget will be shown for that user (or excluded, depending on the setting above).'),
    ];

    $form['debug'] = [
      '#type' => 'details',
      '#title' => $this->t('Debug'),
    ];

    $crownpeak_autofix_js_suffix = $this->state->get('crownpeak_autofix_js_suffix') ?: NULL;
    $crownpeak_autofix_current_js = CROWNPEAK_AUTOFIX_JS_PATH . '/crownpeak_autofix_' . $crownpeak_autofix_js_suffix . '.js';

    $form['debug']['info'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Current generated JS'),
      '#markup' => $this->fileUrlGenerator->generateAbsoluteString($crownpeak_autofix_current_js),
    ];

    $crownpeak_autofix_configs = $this->config('crownpeak_autofix.settings');
    $crownpeak_autofix_js = crownpeak_autofix_build_js($crownpeak_autofix_configs);

    $form['debug']['generated_js'] = [
      '#type' => 'details',
      '#title' => $this->t('Generated JS'),
      '#markup' => "<pre>{$crownpeak_autofix_js}</pre>",
    ];

    $form['#attached']['library'][] = 'crownpeak_autofix/crownpeak_autofix.admin';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('crownpeak_autofix.settings')
      ->set('accessibility_statement_link', $values['accessibility_statement_link'])
      ->set('feedback_form_link', $values['feedback_form_link'])
      ->set('trigger_button_type', $values['trigger_button_type'])
      ->set('interface_language', $values['interface_language'])
      ->set('interface_lead_color', $values['interface_lead_color'])
      ->set('trigger_button_color', $values['trigger_button_color'])
      ->set('interface_position', $values['interface_position'])
      ->set('hide_on_mobile', $values['hide_on_mobile'])
      ->set('trigger_horizontal_position', $values['trigger_horizontal_position'])
      ->set('trigger_vertical_position', $values['trigger_vertical_position'])
      ->set('mobile_trigger_horizontal_position', $values['mobile_trigger_horizontal_position'])
      ->set('mobile_trigger_vertical_position', $values['mobile_trigger_vertical_position'])
      ->set('trigger_button_size', $values['trigger_button_size'])
      ->set('mobile_trigger_size', $values['mobile_trigger_size'])
      ->set('trigger_button_shape', $values['trigger_button_shape'])
      ->set('mobile_trigger_shape', $values['mobile_trigger_shape'])
      ->set('trigger_button_icon', $values['trigger_button_icon'])
      ->set('trigger_horizontal_offset', $values['trigger_horizontal_offset'])
      ->set('trigger_vertical_offset', $values['trigger_vertical_offset'])
      ->set('mobile_trigger_horizontal_offset', $values['mobile_trigger_horizontal_offset'])
      ->set('mobile_trigger_vertical_offset', $values['mobile_trigger_vertical_offset'])
      ->set('visibility_request_path_mode', $values['visibility_request_path_mode'])
      ->set('visibility_request_path_pages', $values['visibility_request_path_pages'])
      ->set('visibility_user_role_mode', $values['visibility_user_role_mode'])
      ->set('visibility_user_role_roles', $values['visibility_user_role_roles'])
      ->save();

    drupal_flush_all_caches();
    parent::submitForm($form, $form_state);
  }

}

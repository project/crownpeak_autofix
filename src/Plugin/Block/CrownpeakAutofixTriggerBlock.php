<?php

namespace Drupal\crownpeak_autofix\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Crownpeak Autofix Trigger' Block.
 *
 * @Block(
 *   id = "crownpeak_autofix_trigger_block",
 *   admin_label = @Translation("Crownpeak Autofix Trigger block"),
 *   category = @Translation("Accessibility"),
 * )
 */
class CrownpeakAutofixTriggerBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '',
      '#theme' => 'crownpeak_autofix_trigger_block',
      '#attached' => [
        'library' => [
          'crownpeak_autofix/crownpeak_autofix_widget',
        ],
      ],
    ];
  }

}
